# Les 1 Hands-on introductie Gitlab

In de keten van voortbrenging is de code (als het om software ontwikkeling en configuratie gaat) de belangrijkste asset die meerdere keren door de keten vloeit. Vandaar ook dat deze voortbrenging en de tooling die dit faceliteert ook wel de pipeline wordt genoemd. Onderstaand voorbeeld laat deze stroom zien op basis van de functies van de ontwikkelwerkplek via de centrale code opslag en het testen tot de uiteindelijke code deployment.

Gitlab werkt conceptueel gezien op basis van een 10-stappen workflow, van Idee tot Productie. Gitlab kan als een CI/CD suite ingezet worden en is hiermee een concurrent van Github en andere aanbieders.
![idea-to-production-10-steps](Les 1/idea-to-production-10-steps.png)

Idee vorming kan je op verschillende manieren starten, hiervoor kan je bijvoorbeeld onze Quint Design Thinking aanpak hanteren. Verder kan een Idee ontstaan tijdens een chat of email uitwisseling. De eerste stap na het identificeren van een Idee, is dat je het Idee onderbrengt in een Issue in Gitlab. Hiermee maak je de discussie en doorontwikkeling van het Idee inzichtelijk en traceerbaar, waarbij je gebruik kan maken van de mogelijkheden om een kanban-stijl bord te gebruiken voor de Plan fase en issue tracking tools, om code of tekst aan het issue te koppelen in de Code fase. 

Afhankelijk van het soort Issue kan er na Commit ofwel een geautomatiseerde build en test plaatsvinden, of een handmatig test door iemand anders zonder extra instructies jou project te laten uitvoeren. Ook hierbij kan gebruik gemaakt worden van de Gitlab tooling om issue te rapporteren en of een oplossing voor te stellen.

Voor meer uitleg over de Gitlab Workflow kan je de volgende blog lezen: [gitlab workflow an overview](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)


## Oefening 1.1
Maak je eerste issue aan door het volgende uit te voeren in je eigen repository:
1. Aan de linkerkant kies je issue (Tip: rechtsklik en open in nieuw tabblad, zodat je eenvoudig naar deze pagina kan..)
2. Klik op New Issue (Groene knop rechtsboven)
3. Geef het Issue een titel en een korte beschrijving, elk team heeft zijn eigen issue bij start van de training gekregen.
4. Bij assignee geef je je eigen naam op, de rest zoals milestone kan je leeg laten..
5. Kijk even rond hoe je issue er in de lijst en op de boards er uitziet en kijk bij de milestones welke al gedefinieerd zijn..

## Oefeing 1.2
Controleer wat de status van je issue is en volg de instructies op die als antwoord op je issue staan.

<instructeur neemt 2 minuten de tijd om de issues te beantwoorden en geeft dan opdracht voor aanpassing, create branche, maak de wijziging en voer een merge requests uit>

## Oefening 1.3
We bespreken in de groep de merge conflicten die zijn ontstaan en hoe deze op te lossen zijn.

# Uitleg
Branching is binnen het ontwikkel proces een methode om verder te werken aan een aftakking, om deze op een gegeven moment weer terug te mergen. In de slides gaan we dieper in op de verschillende stijlen die hiervoor te hanteren zijn.


[Les 2](../Les 2)