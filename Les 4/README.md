# Les 4 Provisioning
Voor provisioning maken we gebruik van Google Cloud GKE en specifiek werken we met de Google Cloud Console. Deze  wordt gebruikt om op basis van immutable infrastructuur principes omgevingen neer te zetten en conform de functionele specificaties beschikbaar te houden. 

Voor deze oefening gebruiken we de container cluster en orchestration dienst van Google gebaseed op Kubernetes, waarin we onze web pagina en containers middels een API koppeling laten landen. De configuratie bestanden houden we ook onder versie controle in Gitlab, zodat zowel functioneel als cloud configuratie 1 geheel vormen.

[![Open this project in Cloud
Shell](http://gstatic.com/cloudssh/images/open-btn.png)](https://console.cloud.google.com/cloudshell/open?git_repo=https://github.com/GoogleCloudPlatform/cloud-shell-tutorials.git&page=editor&tutorial=tutorial.md)

## Oefening 4.1
Google Cloud Console inzetten voor automatisering van provisioning (infra as code), structuur van de deployment file bekijken die we gaan bewerken om de containers op GKE te starten. De oefening wordt uitgevoerd als tutorial in de Cloud Console omgeving, klik hiervoor op de onderstaande button.

## Oefening 4.2
Gooi via de interface van GCP een container weg en zie wat er gebeurt..

## Oefening 4.3
Opruimen van de container






- [cloud shell tutorials](https://cloud.google.com/shell/docs/tutorials)
- [github repo](https://github.com/GoogleCloudPlatform/cloud-shell-tutorials)