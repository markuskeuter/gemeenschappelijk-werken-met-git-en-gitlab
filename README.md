# Gemeenschappelijk werken met git en GitLab

Dit project kan gebruikt worden als start van een CI/CD training, met name hoe het voortbrengingsproces er vanuit ontwikkelaars perspectief er uitziet. Hiervoor starten we vanuit Gitlab en gaan we steeds dieper in op de rol van automatisering in de context van CI/CD. 

- [Les 1 Hands-on introductie Gitlab](Les 1)
- [Les 2 Build en Deploy](Les 2)
- [Les 3 Hands-on Deployment putting it all together](Les 3)
- [Les 4 Provisioning.. de bodem](Les 4)
- [Les 5 Orchestration](Les 5)
- [Les 6 Monitoring en Security](Les 6)

Voor deze training wordt vanuit Quint voorzien in de juiste toegang tot de tooling, deze wordt vanuit public cloud diensten van GitLab.com, Google GCP afgenomen. Mocht je deze oefeningen in eigen tempo zelf willen uitvoeren dan is het mogelijk een trial account aan te maken bij Google en de gratis versie van GitLab in te zetten. Er zal de komende tijd ook een Webinar van deze training beschikbaar komen, hierbij zal de instructie voor het initieel opzetten van de tooling worden meegeleverd.


## Voor instructeurs of mensen die zelf aan de slag willen met dit repository
Door dit project te Forken (speciaal soort kopieren doormiddel van een afsplitsing), kan je de opdrachten (laten) uitvoeren zonder het basismateriaal onbruikbaar te maken. Zoals aangegeven komt er ook een webinar versie van deze cursus, hiervoor moeten echter een aantal zaken extra beschreven worden.

Als eerste stap dus een fork maken van dit project:

![](Video/Fork_a_project.mp4)